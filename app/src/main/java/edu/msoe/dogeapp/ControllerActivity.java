package edu.msoe.dogeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ControllerActivity extends AppCompatActivity {

    protected TextView connectionString;
    protected Button menuButton, walkForwardButton, walkBackwardButton, turnLeftButton,
            turnRightButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);

        connectionString = (TextView) findViewById(R.id.connection_status);

        walkForwardButton = (Button) findViewById(R.id.walk_forward_button);
        walkBackwardButton = (Button) findViewById(R.id.walk_backward_button);
        turnLeftButton = (Button) findViewById(R.id.turn_left_button);
        turnRightButton = (Button) findViewById(R.id.turn_right_button);
        menuButton = (Button) findViewById(R.id.back_button);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.putExtra("connectionStatus", connectionString.getText().toString());
                startActivity(intent);
            }
        });

        walkForwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Tell DogeBot to walk
                /*******************************/
                Toast.makeText(getApplicationContext(),
                        "DogeBot has not been trained to respond yet.", Toast.LENGTH_SHORT).show();
                /*******************************/
            }
        });

        walkBackwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Tell DogeBot to walk
                /*******************************/
                Toast.makeText(getApplicationContext(),
                        "DogeBot has not been trained to respond yet.", Toast.LENGTH_SHORT).show();
                /*******************************/
            }
        });

        turnLeftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Tell DogeBot to turn
                /*******************************/
                Toast.makeText(getApplicationContext(),
                        "DogeBot has not been trained to respond yet.", Toast.LENGTH_SHORT).show();
                /*******************************/
            }
        });

        turnRightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Tell DogeBot to turn
                /*******************************/
                Toast.makeText(getApplicationContext(),
                        "DogeBot has not been trained to respond yet.", Toast.LENGTH_SHORT).show();
                /*******************************/
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("connectionStatus")) {
            String connectionStatus = intent.getExtras().getString("connectionStatus");
            connectionString.setText(connectionStatus);
        }
    }

}