package edu.msoe.dogeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class StatisticsActivity extends AppCompatActivity {

    protected Button menuButton;
    protected TextView connectionString, xOrientation, yOrientation, zOrientation, position,
            batteryLevel;

    private NumberFormat decimalFormat = new DecimalFormat("#.00");
    private NumberFormat percentFormat = new DecimalFormat("#");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        connectionString = (TextView) findViewById(R.id.connection_status);
        xOrientation = (TextView) findViewById(R.id.x_orientation);
        yOrientation = (TextView) findViewById(R.id.y_orientation);
        zOrientation = (TextView) findViewById(R.id.z_orientation);
        position = (TextView) findViewById(R.id.position);
        batteryLevel = (TextView) findViewById(R.id.battery_level);

        menuButton = (Button) findViewById(R.id.back_button);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                intent.putExtra("connectionStatus", connectionString.getText().toString());
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("connectionStatus")) {
            String connectionStatus = intent.getExtras().getString("connectionStatus");
            connectionString.setText(connectionStatus);
        }

        double[] coordinates = getPositionInfo();
        setxOrientation(coordinates[0]);
        setyOrientation(coordinates[1]);
        setzOrientation(coordinates[2]);

        double[] location = getLocation();
        setPosition(location[0], location[1]);

        double batteryLevel = getBatteryLevel();
        setBatteryLevel(batteryLevel);
    }

    protected double[] getLocation() {
        // TODO: Get longitude and latitude
        /*******************************/
        double[] location = {33.221, -56.99};
        /*******************************/
        return location;
    }

    protected double[] getPositionInfo() {
        // TODO: Get positional information
        /*******************************/
        double[] coordinates = {91.56, 88.26, 4.33};
        /*******************************/
        return coordinates;
    }

    protected double getBatteryLevel() {
        // TODO: Get remaining battery percentage
        /*******************************/
        double batteryLevel = 56.31;
        /*******************************/
        return (int) Math.round(batteryLevel);
    }

    protected void setPosition(Double latitude, Double longitude) {
        String lat = decimalFormat.format(latitude);
        String lon = decimalFormat.format(longitude);
        position.setText("Position: (" + lat + ", " + lon + ")");
    }

    protected void setBatteryLevel(Double value) {
        String val = percentFormat.format(value);
        batteryLevel.setText("Battery Level: " + val + "%");
    }

    protected void setxOrientation(Double value) {
        String val = decimalFormat.format(value);
        xOrientation.setText("X Orientation: " + val + "°");
    }

    protected void setyOrientation(Double value) {
        String val = decimalFormat.format(value);
        yOrientation.setText("Y Orientation: " + val + "°");
    }

    protected void setzOrientation(Double value) {
        String val = decimalFormat.format(value);
        zOrientation.setText("Z Orientation: " + val + "°");
    }

}