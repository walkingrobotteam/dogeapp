package edu.msoe.dogeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ConnectActivity extends AppCompatActivity {

    protected Button searchButton;
    protected ListView bluetoothList;
    protected TextView connectionString;
    protected List<String> resultsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        searchButton = (Button) findViewById(R.id.search_button);
        bluetoothList = (ListView) findViewById(R.id.bluetooth_list);
        connectionString = (TextView) findViewById(R.id.connection_status);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean foundBots;

                // TODO: Search for DogeBots
                /*******************************/
                resultsList.clear();
                resultsList.add("DogeBot #3407");
                resultsList.add("DogeBot #5987");
                resultsList.add("DogeBot #2113");
                foundBots = true;
                /*******************************/

                if (foundBots) {
                    bluetoothList.setAdapter(new ArrayAdapter<>(v.getContext(),
                            android.R.layout.simple_list_item_1, resultsList));
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Could not find DogeBot(s). Please try again.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        bluetoothList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean couldConnect;

                // TODO: Connect to the DogeBot clicked
                /*******************************/
                couldConnect = true;
                /*******************************/

                if (couldConnect) {
                    connectionString.setText(R.string.connection_status_connected);
                    Intent intent = new Intent(view.getContext(), MainActivity.class);
                    intent.putExtra("connectionStatus", connectionString.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Could not connect to DogeBot. Please try again.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("connectionStatus")) {
            String connectionStatus = intent.getExtras().getString("connectionStatus");
            connectionString.setText(connectionStatus);
        }
    }

}