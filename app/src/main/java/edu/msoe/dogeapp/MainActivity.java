package edu.msoe.dogeapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    protected TextView connectionString;
    protected TextView xOrientation, yOrientation, zOrientation;
    protected Button controllerButton, statisticsButton, disconnectButton;

    private NumberFormat decimalFormat = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectionString = (TextView) findViewById(R.id.connection_status);
        xOrientation = (TextView) findViewById(R.id.x_orientation);
        yOrientation = (TextView) findViewById(R.id.y_orientation);
        zOrientation = (TextView) findViewById(R.id.z_orientation);

        controllerButton = (Button) findViewById(R.id.controller_button);
        statisticsButton = (Button) findViewById(R.id.statistics_button);
        disconnectButton = (Button) findViewById(R.id.disconnect_button);

        controllerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ControllerActivity.class);
                intent.putExtra("connectionStatus", connectionString.getText().toString());
                startActivity(intent);
            }
        });

        statisticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), StatisticsActivity.class);
                intent.putExtra("connectionStatus", connectionString.getText().toString());
                startActivity(intent);
            }
        });

        disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean couldDisconnect;

                // TODO: Disconnect from DogeBot
                /*******************************/
                couldDisconnect = true;
                /*******************************/

                if (couldDisconnect) {
                    connectionString.setText(R.string.connection_status);
                    Intent intent = new Intent(v.getContext(), ConnectActivity.class);
                    intent.putExtra("connectionStatus", connectionString.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Could not disconnect from DogeBot. Please try again.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("connectionStatus")) {
            String connectionStatus = intent.getExtras().getString("connectionStatus");
            connectionString.setText(connectionStatus);
        }

        double[] coordinates = getPositionInfo();
        setxOrientation(coordinates[0]);
        setyOrientation(coordinates[1]);
        setzOrientation(coordinates[2]);
    }

    protected double[] getPositionInfo() {
        // TODO: Get positional information
        /*******************************/
        double[] coordinates = {91.56, 88.26, 4.33};
        /*******************************/
        return coordinates;
    }

    protected void setxOrientation(Double value) {
        String val = decimalFormat.format(value);
        xOrientation.setText("X Orientation: " + val + "°");
    }

    protected void setyOrientation(Double value) {
        String val = decimalFormat.format(value);
        yOrientation.setText("Y Orientation: " + val + "°");
    }

    protected void setzOrientation(Double value) {
        String val = decimalFormat.format(value);
        zOrientation.setText("Z Orientation: " + val + "°");
    }

}